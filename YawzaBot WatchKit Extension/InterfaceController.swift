//
//  InterfaceController.swift
//  YawzaBot WatchKit Extension
//
//  Created by Maxim Shmotin on 01/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import WatchKit
import Foundation
import BtceApiKit


class InterfaceController: WKInterfaceController {
    @IBOutlet var buyLabel: WKInterfaceLabel!
    @IBOutlet var sellLabel: WKInterfaceLabel!
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        NSLog("Watch start")
        updatePrice()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func refresh(){
        updatePrice()
    }
    
    private func updatePrice(){
        ApiHandler.getBuySellRate(){ (buy,sell) in
            if let buyRate = buy {
                if let sellRate = sell{
                    self.buyLabel.setText("Buy: $\(buyRate)")
                    self.sellLabel.setText("Sell: $\(sellRate)")
                }
            }
        }
    }
}
