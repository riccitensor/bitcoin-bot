//
//  GlanceController.swift
//  YawzaBot WatchKit Extension
//
//  Created by Maxim Shmotin on 01/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import WatchKit
import Foundation
import BtceApiKit


class GlanceController: WKInterfaceController {
    @IBOutlet var buyLabel: WKInterfaceLabel!
    @IBOutlet var sellLabel: WKInterfaceLabel!

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        updatePrice()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    private func updatePrice(){
        ApiHandler.getBuySellRate(){ (buy,sell) in
            if let buyRate = buy {
                if let sellRate = sell{
                    self.buyLabel.setText("$\(buyRate)")
                    self.sellLabel.setText("$\(sellRate)")
                }
            }
        }
    }

}
