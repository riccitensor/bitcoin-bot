//
//  QuotesCustomCell.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 17/12/14.
//  Copyright (c) 2014 Planemo. All rights reserved.
//

import UIKit

class QuotesCustomCell: UITableViewCell {
    @IBOutlet var currencyCode: UILabel!
    @IBOutlet var spread: UILabel!
    @IBOutlet var lastAsk: UILabel!
    @IBOutlet var lastBid: UILabel!
    @IBOutlet var high: UILabel!
    @IBOutlet var low: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
