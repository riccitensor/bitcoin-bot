//
//  WithdrawViewController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 29/04/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//


class WithdrawViewController: UIViewController, QRCodeReaderDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    @IBOutlet var amount: UITextField!
    @IBOutlet var address: UITextField!
    @IBOutlet var fundsLabel: UILabel!
    @IBOutlet var currencyLabel: UILabel!
    
    private var pickerVC = RMPickerViewController.init(style: RMActionControllerStyle.White, selectAction: nil, andCancelAction: nil)
    private var funds = Dictionary<String, Double>()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerVC.picker.delegate = self
        pickerVC.picker.dataSource = self
        pickerVC.picker.resignFirstResponder()
        let cancelAction = RMAction(title: "Cancel", style: RMActionStyle.Cancel, andHandler: nil)
        
        let selectAction = RMAction(title: "Done", style: RMActionStyle.Done) { controller in
            let pickerView = (controller as! RMPickerViewController).picker
            self.currencyLabel.text = "\(Array(self.funds.keys)[pickerView.selectedRowInComponent(0)].uppercaseString)"
            let currencyCode = self.currencyLabel.text!
            if self.funds.keys.count > 0 {
              self.fundsLabel.text = "\((self.funds[currencyCode.lowercaseString]?.format())!)"
            }
        }
        pickerVC.addAction(cancelAction)
        pickerVC.addAction(selectAction)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.updateBalance() { handler in}
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Picker View
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return funds.keys.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return "\((Array(funds.keys)[row]).uppercaseString)"
    }

    
    @IBAction func openDateSelectionController(sender: UIButton){
        if self.funds.keys.count > 0 {
            self.presentViewController(pickerVC, animated: true, completion: nil)
        }
    }

    
    // Withdraw
    
    @IBAction func withdraw(sender: UIButton){
        if let json = BtceApiHandler.sharedInstanse.withdrawCoin(self.currencyLabel.text!, amount: self.amount.text!, address: self.address.text!){
            self.amount.text = ""
            if let response: AnyObject = json["return"], tranId = response["tId"] as? Int, amountSent = response["amountSent"] as? Double{
                self.updateBalance(){ (completionHandler) in }
                Dialog.show(Dialog.Title.SUCCESS, message: "Transaction ID: \(tranId) \n Amount Sent: \(amountSent)")
            }else if let _ = json["error"] as? String{
                dispatch_async(dispatch_get_main_queue()) {
                    Dialog.show(Dialog.Title.ERROR, message: (json["error"] as! String).capitalizeFirst)
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
    // Scanner
    
    @IBAction func scanQRcode(sender: UIButton){
        let reader = QRCodeReaderViewController()
        reader.modalPresentationStyle = UIModalPresentationStyle.FormSheet
        reader.delegate = self
        self.presentViewController(reader, animated: true, completion: nil)
    }
    
    func reader(reader: QRCodeReaderViewController!, didScanResult result: String!) {
        self.dismissViewControllerAnimated(true, completion: {
            self.address.text = result.extractBitcoinAddress()
        })
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Update Balance
    
    func updateBalance(completionHandler: Bool -> Void){
        print("run update balance")
        dispatch_async(BtceApiHandler.sharedInstanse.dispatch_serial_queue){
            if let json = BtceApiHandler.sharedInstanse.updateAccountInfo(){
                if let info = json["return"] as? NSDictionary, funds = info["funds"] as? NSDictionary {
                    self.funds = funds as! Dictionary
                    self.pickerVC.resignFirstResponder()
                    let currencyCode = self.currencyLabel.text!
                    self.fundsLabel.text = "\((self.funds[currencyCode.lowercaseString]?.format())!)"
                    completionHandler(true)
                }else if let error = json["error"] as? String{
                    dispatch_async(dispatch_get_main_queue()) {
                        Dialog.show(Dialog.Title.ERROR, message: (json["error"] as! String).capitalizeFirst)
                        SVProgressHUD.dismiss()
                    }
                }
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
