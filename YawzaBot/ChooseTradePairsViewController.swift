//
//  ChooseTradePairsViewController.swift
//  YawzaBot
//
//  Created by Maxim Shmotin on 07/02/15.
//  Copyright (c) 2015 Planemo. All rights reserved.
//

import UIKit

class ChooseTradePairsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    private var newCodes = Dictionary<Int, String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func done(sender: UIButton){
        Currency.appendAtIndex(Array(newCodes.values))
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    

    // Table view methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Currency.getCodesCount()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = Currency.toCodeTitle(indexPath.row)
        if (Currency.containsAllCodes(indexPath.row)){
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if (cell?.accessoryType == UITableViewCellAccessoryType.None){
            cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
            newCodes[indexPath.row] = Currency.getCode(indexPath.row)
        } else if (!Currency.containsAllCodes(indexPath.row)){
            cell?.accessoryType = UITableViewCellAccessoryType.None
            newCodes.removeValueForKey(indexPath.row)
        }
    }
}
